# Spores

## Histoires

L'Apocalypse ne s'est pas passée comme prévue. La désertification et la
fonte des glaces n'ont pas continué. Le réchauffement climatique
climatique n'a pas eu lieu ... car la nature a rendu coups pour coups.

En 2025 de nouvelles plantes et champignons sont soudainement apparus un
peu partout dans le monde. Leur croissance fut extraordinairement
rapide, certains d'entre elles ayant rapidement atteint les dix mètres
de haut. Vues comme des curiosités et comme des conséquences en
apparence inoffensive d'un réchauffement climatique qui commençait à
exacerber les tensions sociales ( vagues de migrations dues à la famine
; premières guerres civiles pour les points d'eau potables dans les pays
les plus touchés par le début de désertification ... ), ces nouvelles
plantes attirèrent l'attention des scientifiques des pays encore
relativement épargnés par la pollution.

Un jour, tout bascula. Le 16 décembre 2025 entre 9h et 12h TUC, ces
plantes et champignons libérèrent leurs spores de manière d'autant plus
violente qu'au même moment de terribles tempêtes s'abattirent sur toute
la surface du globe.

Ces spores s'infiltrèrent partout et contaminèrent tous les être
vivants, mais aussi les bâtiments et les objets. C'est ainsi qu'en
l'espace d'une journée la quasi totalité du monde fut recouvert par ces
spores.

Le monde fut saisi de peur, des programmes massifs de "décontamination"
furent lancés contre un potentiel ennemi dont on ne savait rien. Ce
n'est qu'au bout d'une semaine que les premières conséquences de cette
contamination apparurent : des champignons et autres plantes fongiques
commencèrent à apparaître sur les voitures, les murs des maisons, les
lignes électriques ... Des gens commencèrent à tousser, à cracher du
mucus de couleurs verdâtre, à tomber malades.

Le quatorzième jour, malgré les essais de décontamination, on
comptabilisa trois milliards de morts recouverts de champignons, et un
milliard de personnes ayant survécus avec des séquelles notables ( tous
recouverts par endroit de champignons ou autres plantes fongiques à un
degré très profond ), souvent rendus débiles à cause des atteintes
cérébrales, et cent millions d'entre eux devinrent extrêmement violents
à l'égard des autres êtres vivants.

Le vingt-et-unième jour la plupart des centres de production électrique
s'arrêtèrent : soit parce que les spores avaient contaminés les
bâtiments et progressivement détruits les systèmes informatiques et
autres matériels , soit parce qu'il y avait eu trop de morts parmi les
employés. On commença à couper l'alimentation des centrales nucléaires
sur l'ensemble du globe. Dans les jours qui suivirent la mort de
nombreux dirigeants, de médecins, les catastrophes sanitaires dues au
nombre de morts, tout cela entraîna le monde dans l'anarchie.

Le vingt-huitième jour le monde fut frappé par l'explosion plusieurs
centrales nucléaires dans le monde et par les fuites radioactives de
nombreuses autres ayant été arrêtées en urgence. En plus de cela on
s'aperçut que de nouvelles morts et autres mutations apparurent parmi
les personnes ayant "survécus" aux deux premières semaines. La
progression des spores dans l'organisme semblait très variable d'un
individu à un autre.

Au bout d'un mois les organes de presse étaient réduits à néant, les
gens s'étaient cloîtrés dans leurs maisons ou des bunkers, les
gouvernements étaient tombés. Les nouvelles du monde devinrent rares et
imprécises. On entendait parler d'îlots relativement épargnés par la
catastrophe en bord de mer ici et là, et c'était à peu près tout. Les
quelques survivants, que l'on estimait environ à un milliard sur toute
la surface du globe, sans compter les quelques centaines de millions de
"mutants" qui erraient à la recherche d'être vivants à dévorer,
apprenaient à vivre dans ce nouveau monde.

## Le Finistère en 2075

...
