---
title: Notes sur l'univers de Troïka
---
# Peuples

**Géant de Corda** : Passionnés, sensibles, conteurs, épicuriens. Ils sont les auteurs des fameuses *Cartes Stellaires Schématiques de Corda*, grande de plusieurs mètres carrés, et contenant les secrets du voyage entre les sphères.

**Nain** : Créatures sculptées par d'autres nains. But : créer des œuvres d'art dans des endroits inhabituels. L'œuvre ultime d'un nain est un autre nain taillé dans un rocher. Parfois ça rate et ils fabriquent un Nain Contrefait.

# Créatures

**Ceux-Sales-Nés** : Engence monstrueuse née dans des lieux obscurs, loin des civilisations.

**Démons** : Craignent l'argent. S'enduire de leur sang rend invisible à leurs yeux.

**Les gremlins** : Il y a des chasseurs qui s'occupent spécialement d'eux.

**Pollueurs des étangs** : grand prêtres adeptes de P!P!Ssshrp le Dieu crapaud bouffi. Vivent avec les mouches.

**Homme-Rino** : Créatures anciennement issues de la sorcellerie qui se mettent au service de puissants. Ils sont, pour une raison que l'on ignore, porteur d'un heaume minuscule et d'une pique tout aussi petite, et aiment jouer avec leurs dés en os (probablement en os de gobelins).

**Lamma Sceptique** : Créatures coiffées d'un chapeau tambourin et disposant d'un corps de taureau, d'une tête humaine, de pattes de chat et d'ailes de cygnes. Créatures douces d'essence divines, descendues des cieux pour traverser les abysses et rejoindre les étoiles.

# Académies, guildes et sociétés

**L'Académie des Portes** : Prestigieuse académie de Troïka formant des sorciers experts en mobilité pandimensionnelle. De ce fait, le sort *Envergure astrale*, permettant une translocation partielle d'un sorcier grâce à la connaissance d'un réceptacle dans le lieu visé, a rendu célèbre l'Académie.

**Astrologues** : L'étude de l'astrologie est essentielle pour dresser les cartes stellaires et déterminer la destination des portails interdimensionnels.

**Les Chevaliers de l'Horizon** : Chevaliers probablement aujourd'hui disparus qui combattaient le Rien. Pour ce faire ils usaient du sort *Ligne de vie* pour faire porter leurs fonctions vitales à leurs écuyers afin de se jeter dans le néant des bords de la création.

**Les Clavistes** : Ce sont les maîtres des clés. Ils parcourent l'univers pour étudier leur unique passion : les clés, aussi bien physiques que métaphoriques.

**Le Club des gourmets de Mademoiselle Kinsey** : Ses membres se nomment les *Mangeurs* et n'ont d'autre but que d'ingérer tout ce que le Monde Extérieur a à leur proposer dans leur Monde Intérieur. Ils disposent pour cela de dentiers métalliques de diverses formes et fonctions.

**Les Époptes** : devins itinérants habillés d'une tenue jaune. Utilise un bâton à l'extrémité duquel est serti un Rubis trouble, une loupe octroyant la Seconde Vue, mais aussi une arme et un emblême.

**Les Exographes** : Universitaires traversant les sphères, habillés de leur combinaison hermétique en caoutchouc, pour étudier les régions éloignées des effluves divins.

**La Guide des Angles Aigus** : Forme des assassins.

**La Guilde des Portefaix** : Ses membres se mettent au service des autres pour supporter toute charge. Ils parcouent les sphères pour servir toujours plus.

**Lansquenets** : Mercenaires engagés par le Trône du Phénix et envoyés en Barges Dorés vers des sphères lointaines pour répandre, par l'épée, la gloire de leurs seigneurs. Habillés de couleurs vives.

**La Société de Knidos** : Ses *mathémologues* étudient tout à l'aune de la mathémologie afin de percer la surface éthérée pour entrevoir les nombres fondamentaux qui structurent toute chose.

**Les Sorcières de Papier** : Sorcières mortent depuis longtemps et couvrant leur peau en décomposition par une reproduction parfaite en papier.

**La Sublime Société des Biftèques** : Ses compagnons se nomment les *Bagarreurs*, à raison car ils n'aiment que deux choses : la force et un bon steak.

**L'Université des Amis** : Portant aussi le nom d'Académie Subdimensionnelle du Dieu-sorcier Cordial. Forme des sorciers au savoir théorique considérable mais peu au fait de ce qui se passe au-dehors. Il n'est pas rare que ses apprentis s'ennivrent le soir dans les bars et les bordels, raison pour laquelle l'Université envoie ses *Factotums* les débarasser de ces lieux.

**Zoanthropologue** : Curieux savant étudiant, élevant et créant des zoanthropes en retirant le cerveau de toute personnes désireuse de se libérer du poid des vanités et de s'élever au rang d'un simple animal.

# Temples et autres églises

**L'Église de la Rédemption Rouge** : Ses évangélistes antimatérialistes sont des confesseurs itinérants cherchant à extirper partout le péché associé à la matière terrestre. Accumuler et consommer la Masse nous densifie et nous lie au démiurge, ce dont il faut se libérer. Ils sont habillés d'une tenu rouge, coiffés d'un heaume de métal sans visage et porteurs d'un grande hache qui n'a de fonction que symbolique. Pour ses Prêtres tout comportement perçu comme négatif signe un certain degré de possession ou d'influence par les forces du Changement qui sont des agents involontaires de la Masse.

**Temple de Télak l'Épéiste** : Ses chevaliers sont des moines fanatiques se préparant pour la fin des temps. Ils portent *la bénédiction de Télak*, une armure qui tire sa puissance du nombre d'épées que porte le chevalier. Ils usent de ces épées d'argent pour atteindre à distance leurs ennemis grâce au sort *Frappe insensible*.

# Villes, cités et autres sphères

**La Cité de Laiton** : Cente de savoir prestigieux.

**Le Palais des Tigres** : Centre de savoir prestigieux.

**Troïka!** : Si ça n'est pas précisé c'est que ça se passe à Troïka ! L'une des traditions de ses habitants consiste à consulter un Baromètre de Poche pour obtenir une prévision météorologique que l'on commentera avec son interlocuteur. La cérémonie du thé impressionne beaucoup les habitants de cette ville.

**Les villages du Mur** : Ces villages sont posés en haut de murs immenses et instables. Leurs résidents aiment manger les singes que leurs apportent les Colporteurs de Singes.

**Yongardy** : Ville curieuse où les gens respectent fondamentalement la Loi. L'administration est au centre de leur vie, les avocats sont leurs héros.

# De la matière du monde

**l'Argent** : Métal le plus pur issu des étoiles du ciel convexe. Il peut blesser les créatures immunisées aux blessures physiques (démons et morts-vivants principalement, plus efficace que le sel qui ne peut pas les tuer).

**Lumière astrale** : Accessible en traversant le voile de la réalité, y naviguer permet de transcender tous les problèmes d'orientation habituels.

**Noyaux Plasmiques** : Cristal serti dans un métal servant de source d'énergie pour les armes exotiques et dont la nature est incertaine (lumière stellaire ? Vapeurs astrales ? Fantômes solidifiés ?).

# Objets curieux ou importants

**Barges Dorées** : Elles servent à naviguer entre d'une sphère à l'autre.

**Velare** : Bijou fantaisie permettant de produire un déguisement complet fait de lumière solide (vous rêvez de jouer une Magical Girl ?).
