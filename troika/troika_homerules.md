---
title: Troika! & AFF homerules
---

# AFF & Troika!

## Utilisation des caractéristiques

**Habileté** : Dans AFF : score fixe à la création de 6 (pour éviter les trop grands écarts entre les joueurs). Ou appliquer la règle de Troika! : 1d3 + 3.

L'Habileté ne peut être utilisée que si on a la compétence.

**Chance** : À utiliser pour toute action dans laquelle on n'est pas compétent.

## Systèmes de résolution unifiés

### Tout passer en "roll over"

Passer en roll over implique d'établir un score fixe. Une base de 14 est recommandée par de nombreux joueurs, mais on peut la réduire ou l'augmenter pour modifier la difficulté du jet.

**Test sans opposition / Test sous** : 2D6 + Habileté + Compétence >= 14

**Tenter sa chance / Test sous** : 2D6 + Chance >= 14

**Échec critique** : toujours double 1.

**Réussite critique** : Toujours double 6.

### Tout passer en test d'opposition

Faire un test de chance ou d'habileté (test sous) suit toujours les règles du test en opposition (test contre) avec un score adverse de base de 6 (non testé).

# AFF seulement

**Lancer des sorts** 2D6 + Compétence + Magie >= 14 & critique sur double 1

Remplacer les + par des moins, et inversement, dans les tableaux de modificateurs.

**Chuter** : Ajouter +1 à la difficulté (base : 14) pour chaque 5 mètres.

**Augmenter la Magie** : 20x le nouveau score à atteindre.

**Augmenter une compétence** : 5x le nouveau score à atteindre.

**Nombre de sortilèges connus** : Nbr de niveaux de sort égal à Magie + Magie Supérieure.
