# Présentation des obscurs grimoires

**notes_univers_troika** : Compilation des notes sur l'univers de Troika! disséminées dans le livre de base.

**troika_homerules** : Quelques règles personnelles (et récupérées ici et là) pour les jeux basés sur le moteur AFF.

# Ressources éparpillées sur la Toile des Sphères

- [Troika! sur CasusNo](https://www.casusno.fr/viewtopic.php?t=40593)

- [Tous les publications de Melsonian Arts Council](https://www.melsonia.com/all-books-16-c.asp)

- [Patern Recog, l'éditeur en charge de la traduction française](https://patternrecogeditions.com/project/)

- [Une fiche de personnage pour l'édition française](https://www.whidou.fr/une-fiche-de-personnage-en-francais-pour-troika.html)

- [La jam itch.io pour les backgrounds](https://itch.io/jam/troika-backgrounds-jam)

- [La jam itch.io pour les sphères](https://itch.io/jam/the-great-troika-pocket-sphere-jam)

- [Jam itch.io pour les aventures](https://itch.io/jam/troika-pamphlet-adventure-jam/entries)
