#!/bin/bash -

date_time=$(date '+%d/%m/%Y %H:%M:%S');

#### Préparer pour envoi #####
shopt -s globstar
for f in ./*.{pdf,html,htm,org,odt,odp,png,jpg,txi,scm,md,bib,csl,csv,epub,sh} ./**/*.{pdf,html,htm,org,odt,odp,png,jpg,txi,scm,md,bib,csl,csv,epub,sh} ; do
  git add "$f"
done

##### Envoyer sur le dépôt git #####
git commit -m "maj $date_time" &&
git push
