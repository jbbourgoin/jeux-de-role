# Homerules DD5

## D&D5 simplifié

### Compétences

On utilise la variante "Maîtrise du test de caractéristique" du DMG en p.263.

- La maîtrise s'applique à deux caractéristiques au choix du joueur.

- Si expertise le joueur l'applique à l'une des deux caractéristiques maîtrisées.

- Si nouvelle maîtrise de compétence, le joueur choisit une nouvelle caractéristique.

*Avantage* : plus souple ; accélère la création de personnage ; réduit la lourdeur de la fiche de personnage.

*Modification personnelle* : le bonus de maîtrise ne s'applique que si cela fait sens par rapport au personnage joué. Par exemple un magicien qui n'a jamais quitté la terre ferme ne pourra pas justifier de sa maîtrise en Intelligence pour trouver un bon chemin de navigation sur une carte maritime, mais un autre qui a un historique de marin le pourra.

### Jets de sauvegarde

Il n'y a plus de distinction entre la maîtrise des jets de sauvegarde et la maîtrise d'une compétence / caractéristique. On utilise la variante des compétences et on considère un jet de sauvegarde comme un test "passif" et un test de compétence comme un test "actif".

*Avantage* : simplification dans l'explication du système et simplification de la fiche de personnage.

### Sorts

Utiliser la variante des points de sort dans le DMG p.268 :

- Le personnage dispose d'un nombre de points de sort en fonction de son niveau de lanceur de sort.

- Un sort coûte un certain nombre de points en fonction de son niveau.

- On peut lancer un seul sort *par niveau* de niveau 6 ou supérieur entre deux repos longs.

- Un repos long permet de récupérer tous les points de sort.

*Avantage* : c'est un système plus facile à expliquer au prix d'un très léger effort de calcul.

### Armement

Les dés de dégât ne sont plus associés à l'arme mais à la maîtrise et à la classe du personnage.

- Classe de type lanceur de sort : 1d4 (sans les bonus liés à l'arme) sans maîtrise ; 1d8 avec maîtrise ; 2d4 si maniée à deux mains et maîtrise.

- Classe de type combattant : 1d8 (sans bonus liés à l'arme) sans maîtrise ; 1d10 avec maîtrise ; 1d12 si maniée à deux mains et maîtrise.

- Plus de propriété "finesse", un combattant utilisant sa Dextérité comme compétence majeure pour le combat, combat avec finesse tant qu'il a la maîtrise de l'arme correspondante.

- On ne note plus "contondant", "tranchant", "allonge", ni même deux mains ou polyvalent ... tout cela est déterminé par la représentation logique que l'on s'en fait.

*Avantage* : simplification de l'écriture de l'arme sur la fiche ; créer une arme à la volée ne consiste plus qu'à déterminer sa catégorie et ses éventuelles caractéristiques ou pouvoirs spéciaux.

### Historique

Ne garder que le titre de l'historique et laisser un petit encadré dans lequel le joueur peut noter quelques traits concernant son personnage (borgne, avare, frère du baron local etc.).

*Avantage* : création plus rapide ; l'encadré peut être complété en cours de jeu, voire être modifié en cours de jeu ; s'interface bien avec le système de simplification des compétences.

### Équipement

Un personnage dispose des outils essentiels à son historique et à sa classe. Il n'a pas besoin de les écrire ni d'y penser en amont. Si une situation appelle l'usage d'un tel objet, le joueur peut l'invoquer comme s'il le possédait depuis le début de la partie.

## Archétypes

### Anglais

Adventurer (Bard, College of Valor)

Alchemist (Wizard, School of Transmutation)

Arcane Trickster (Rogue)

Avenger (Paladin, Oath of Vengeance)

Battle Master (Fighter)

Beast Master (Ranger)

Berserker (Barbarian)

Champion (Fighter)

Cursed Warlock (Warlock, Great Old One Patron)

Dark Knight (Paladin, Oath of Vengeance)

Dark Mage (Warlock, Fiend Patron)

Death Mage(Wizard, School of Necromancy)

Diviner (Wizard, School of Divination)

Draconic Blood Mage (Sorcerer)

Eldritch Knight (Fighter)

Elementalist (Monk, Way of the Four Elements)

Elementalist (Wizard, School of Evocation)

Evil Warlock (Warlock, Fiend Patron)

Fire Mage (Wizard, School of Evocation)

Fortune Teller (Wizard, School of Divination)

Friend of the Fairies (Warlock, Archfey Patron)

Good Witch (Warlock, Archfey Patron)

Guardian Knight (Paladin, Oath of the Ancients)

Guardian of the Wild (Druid, Circle of the Wild)

Healer (Cleric, Life Domain)

Hunter (Ranger)

Illusionist (Wizard, School of Illusion)

Kung-fu Master (Monk, Way of the Open Hand)

Lore Master (Druid, Circle of the Land)

Lore Master (Bard, College of Lore)

Meddler in the Dark Arts (Warlock, Great Old One Patron)

Necromancer (Cleric, Death Domain)

Necromancer (Wizard, School of Necromancy)

Ninja (Monk, Way of the Shadows)

Priest of Knowledge (Cleric)

Priest of Life (Cleric)

Priest of Light (Cleric)

Priest of Nature (Cleric)

Priest of Trickery (Cleric)

Priest of War (Cleric)

Protector (Wizard, School of Abjuration)

Questing Knight (Paladin, Oath of Devotion)

Storm Priest (Cleric, Tempest Domain)

Thief (Rogue)

Totem Warrior (Barbarian)

Transmutator (Wizard, School of Transmutation)

Wild Mage (Sorcerer, Wild Magic)

Witch (Warlock, Fiend Patron)

Wizard (Wizard, School of Evocation)

Autres :

Assassin (Monk, Way of Shadow)

Assassin (Rogue)

Barbarian

Cavalier (Paladin, Oath of Devotion)

Court Minstrel (Bard, College of Lore)

Druid

Duelist (Fighter, Champion)

Fey Knight (Paladin, Oath of Ancients)

Green Knight (Paladin, Oath of Ancients)

Hermit (Cleric, Nature Domain)

Hermit (Druid, Circle of the Moon)

Holy Warrior (Paladin, Oath of Devotion)

Horned Knight (Paladin, Oath of Ancients)

Lore Master (Cleric, Knowledge Domain)

Ranger

Scholar (Bard, College of Lore)

Scholar (Cleric, Knowledge Domain)

Shadow Dancer (Monk, Way of Shadow)

Spy (Monk, Way of Shadow)

Spy (Rogue, Assassin)

Sun Priest (Cleric, Light Domain)

Viking (Barbarian, Berserker)

Wandering Minstrel (Bard, College of Valor)

Warlord (Fighter, Battle Master)

Warrior Priest (Cleric, War Domain)

White Knight (Paladin, Oath of Devotion)

### Traduction

Aventurier (Barde, Collège de la Vaillance)

Alchimiste (Sorcier, Ecole de Transmutation)

Arcane Trickster (Rogue)

Vengeur (Paladin, Serment de vengeance)

Maître de bataille (Combattant)

Maître des bêtes (Rôdeur)

Berserker (Barbare)

Champion (Combattant)

Sorcier maudit (Sorcier, parrain du Grand Ancien)

Chevalier noir (Paladin, Serment de vengeance)

Mage noir (Sorcier, parrain du démon)

Mage de la mort (Sorcier, Ecole de Nécromancie)

Divinateur (Sorcier, École de divination)

Draconic Blood Mage (Sorcier)

Eldritch Knight (Combattant)

Élémentaliste (Moine, Voie des quatre éléments)

Élémentaliste (Sorcier, École d'évocation)

Sorcier maléfique (Sorcier, patron des démons)

Mage de feu (Sorcier, École d'évocation)

Diseur de bonne aventure (Sorcier, École de divination)

Ami des fées (Sorcier, parrain de l'archidémon)

Bonne Sorcière (Sorcier, Archiefey Patron)

Chevalier Gardien (Paladin, Serment des Anciens)

Gardien de la nature (Druide, Cercle de la nature)

Guérisseur (Clerc, Life Domain)

Chasseur (Ranger)

Illusionniste (Sorcier, École d'illusion)

Maître de kung-fu (Moine, Voie de la main ouverte)

Maître du savoir (Druide, Cercle de la terre)

Maître du savoir (Barde, Collège du savoir)

Meddler in the Dark Arts (Warlock, Great Old One Patron)

Nécromancien (Clerc, Domaine de la Mort)

Nécromancien (Sorcier, École de Nécromancie)

Ninja (Moine, Voie des Ombres)

Prêtre de la Connaissance (Clerc)

Priest of Life (Clerc)

Priest of Light (Clerc)

Priest of Nature (Cleric)

Priest of Trickery (Cleric)

Prêtre de la Guerre (Clerc)

Protector (Wizard, School of Abjuration)

Questing Knight (Paladin, Serment de Dévotion)

Storm Priest (Clerc, Tempest Domain)

Thief (Rogue)

Totem Warrior (Barbare)

Transmutateur (Sorcier, École de Transmutation)

Wild Mage (Sorcier, Wild Magic)

Sorcier (sorcier, patron du démon)

Sorcier (Sorcier, Ecole d'Evocation)

Autres :

Assassin (Moine, Voie de l'Ombre)

Assassin (Voleur)

Barbare

Cavalier (Paladin, Serment de dévotion)

Ménestrel de cour (Barde, Collège des connaissances)

Druide

Duelliste (Combattant, Champion)

Chevalier féerique (Paladin, Serment des Anciens)

Green Knight (Paladin, Serment des Anciens)

Hermite (Clerc, Domaine de la Nature)

Hermite (Druide, Cercle de la Lune)

Guerrier saint (Paladin, Serment de dévotion)

Horned Knight (Paladin, Serment des Anciens)

Lore Master (Clerc, Knowledge Domain)

Ranger

Scholar (Bard, College of Lore)

Scholar (Clerc, Knowledge Domain)

Shadow Dancer (Monk, Way of Shadow)

Espion (Moine, Voie de l'Ombre)

Espion (Voleur, Assassin)

Sun Priest (Clerc, Light Domain)

Viking (Barbare, Berserker)

Wandering Minstrel (Barde, College of Valor)

Warlord (Combattant, Maître de guerre)

Prêtre guerrier (clerc, domaine de la guerre)

Chevalier blanc (Paladin, Serment de dévotion)

Traduit avec www.DeepL.com/Translator (version gratuite)
